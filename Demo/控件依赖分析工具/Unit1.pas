unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls,System.RegularExpressions,
  Vcl.ComCtrls;

type
  TForm1 = class(TForm)
    Button1: TButton;
    Memo1: TMemo;
    Edit1: TEdit;
    StatusBar1: TStatusBar;
    Edit2: TEdit;
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;



  TPack = record
    PackName:string;
    PackPathFileName:string;
    number:Integer;
    requires:array [0..255] of Pointer;
  end;
  PPack = ^TPack;


var
  Form1: TForm1;
  List:TList;
  LibraryPath:TStringList;
  PackInstallList:TStringList;
  syshavepack:TStringList;

implementation

{$R *.dfm}
function FindPack(Name:string):PPack;
var
  i:Integer;
begin
  Result := nil;
  for i := 0 to List.Count - 1 do
  begin
   if SameText(PPack(List.Items[i]).PackName,Name) then
   begin

    Result := PPack(List.Items[i]);
    Exit;
   end;
  end;
end;


function Findrequires(pack,requirePACK:PPack):Boolean;
var
  I:Integer;
begin
  Result := False;
  for I := 0 to pack.number - 1 do
  BEGIN
    if pack.requires[I] = requirePACK then
      Result := True;
  END;
end;

procedure GetRequires(Filename:string);
const
  R:string = '(?=requires)(.*?);';
  R2:STRING = '[^requires][0-9A-Za-z_]{1,}';
var
  strfile:TStringList;
  REGEX:TRegEx;
  match:TMatch;
  requires:string;
  requiresPack,SelfPack:PPack;
begin
  strfile := TStringList.Create;
  strfile.LoadFromFile(filename);
  REGEX := TREGEX.Create(R,[roSingleLine]);

  //找列表里有没有自己，找不到就创建一个
  SelfPack := FindPack(ChangeFileExt(ExtractFileName(FileName),''));
  if SelfPack = nil then
  begin
    SelfPack := AllocMem(SizeOf(TPack));
    List.Add(SelfPack);
  end;
  SelfPack.PackName := ChangeFileExt(ExtractFileName(FileName),'');
  //设置路径
  SelfPack.PackPathFileName := FileName;
  //判断是否有依赖
  match := REGEX.Match(strfile.Text);
  if match.Success then
    requires := match.Value;
  if requires <> '' then
  begin
    //枚举依赖
    match := TRegEx.Match(requires,R2,[roSingleLine]);
    while match.Success do
    begin
       //查找依赖包
         requiresPack := FindPack(Trim(match.Value));
         //找不到依赖包结构就创建一个
         if requiresPack = nil then
         begin
           requiresPack := AllocMem(SizeOf(TPack));
           requiresPack.PackName := Trim(match.Value);
           List.Add(requiresPack);
         end;
         //查找依赖列表是否已经添加
         if NOT Findrequires(SelfPack,requiresPack) then
         begin
           //添加依赖
           SelfPack.requires[SelfPack.number] := requiresPack;
           Inc(SelfPack.number);
         end;
       match := match.NextMatch;
    end;
  end;
  strfile.Free;
end;
function AddSTR(LIST:TStrings;STR:STRING):Boolean;
var
  I: Integer;
begin
  Result := False;
  for I := 0 to LIST.Count - 1 do
  begin
   if SameText(STR,LIST.Strings[I]) then
   begin
    EXIT;
   end;
  end;
  LIST.Add(STR);
  Result := True;
end;

function IsList(list:TStrings;str:string):Boolean;
var
  I: Integer;
begin
  Result := False;
  for I := 0 to LIST.Count - 1 do
  begin
   if SameText(STR,LIST.Strings[I]) then
   begin
    Result := True;
    EXIT;
   end;
  end;
end;

function requireok(vminstallpack:TStrings;pack:PPack):Boolean;
var
  c:Integer;
begin
       Result := False;
       if not IsList(vminstallpack,pack.PackName) then
       begin
          for c := 0 to pack.number - 1 do
          begin
           if not IsList(vminstallpack,PPack(pack.requires[c]).PackName) then
            exit;
          end;
       end;
       Result := True;
end;

procedure StartAnalysis();
var
  I:Integer;

  vminstallpack:TStringList;

  pack:PPack;
begin

  vminstallpack := TStringList.Create;

 // syshavepack.Add('依赖的外部包：');
  for I := 0 to List.Count - 1 do
  begin
    if PPack(List.Items[i]).PackPathFileName = '' then
    begin
       AddSTR(syshavepack,PPack(List.Items[i]).PackName);
    end;
  end;
  vminstallpack.Text := syshavepack.Text;
  while vminstallpack.Count <> List.Count do
  begin
    for i := 0 to List.Count - 1 do
    begin
       pack := PPack(List.Items[i]);
       if NOT IsList(vminstallpack,pack.PackName) then
       begin
         if requireok(vminstallpack,pack) then
         begin
           //已经可以安装了
           vminstallpack.Add(pack.PackName);
           PackInstallList.Add(pack.PackPathFileName);
         end;
       end;
    end;
  end;


end;

function EnumPathDIR(Path:string):Integer;
var
  SearchRec: TSearchRec;
  found: integer;
  SucceedNum:Integer;
begin
  SucceedNum := 0;
  OutputDebugString(PWideChar(path));
 // EnumPathdpk(path,'*.*');
  found := FindFirst(path + '*', faAnyFile, SearchRec);
  while found = 0 do
  begin
    if (SearchRec.Name <> '.') and
       (SearchRec.Name <> '..') and
       (SearchRec.Attr = faDirectory) then
    begin
      EnumPathDIR(path + SearchRec.Name + '\');
    end
    else
    if (SearchRec.Name <> '.') and
       (SearchRec.Name <> '..') and
       (SearchRec.Attr <> faDirectory) then
    begin
      if SameText(ExtractFileExt(SearchRec.Name),'.dpk') then
      begin
        if TRegEx.Match(SearchRec.Name,Form1.Edit2.Text).Value <> '' then
        GetRequires(Path + SearchRec.Name);
      end
      else
      if SameText(ExtractFileExt(SearchRec.Name),'.pas')
          or SameText(ExtractFileExt(SearchRec.Name),'.inc')
          or SameText(ExtractFileExt(SearchRec.Name),'.obj')
          or SameText(ExtractFileExt(SearchRec.Name),'.res') then
      begin
       if not IsList(LibraryPath,Path) then
          LibraryPath.Add(Path);
      end;
    end;
    found := FindNext(SearchRec);
  end;
  FindClose(SearchRec);
  Result := SucceedNum;
end;

procedure TForm1.Button1Click(Sender: TObject);
var
  I:Integer;
  path:string;
  c:Integer;
begin
    LibraryPath := TStringList.Create;
    PackInstallList := TStringList.Create;
    syshavepack := TStringList.Create;
    Memo1.Clear;
    List := TList.Create;
    path := Edit1.Text;
    if path[Length(path)] <> '\' then
    path := path + '\';
    c := GetTickCount;
    EnumPathDIR(path);

    StartAnalysis;

    Form1.Memo1.Lines.Add('外部依赖包：');
    Form1.Memo1.Lines.Add(syshavepack.Text);

    Form1.Memo1.Lines.Add('搜索路径：');
    Form1.Memo1.Lines.Add(LibraryPath.Text);

    Form1.Memo1.Lines.Add('安装顺序：');
    Form1.Memo1.Lines.Add(PackInstallList.Text);
    Memo1.Lines.Add(Format('耗时：%dms',[GetTickCount - c]));

    list.Free;
    syshavepack.Free;
    PackInstallList.Free;
    LibraryPath.Free;
end;

end.
