unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs,IDEInsetupPacket, Vcl.StdCtrls,QWorker,
  Vcl.Buttons, Vcl.ComCtrls;

type
  TForm1 = class(TForm)
    Memo1: TMemo;
    Memo2: TMemo;
    BitBtn1: TBitBtn;
    StatusBar1: TStatusBar;
    BitBtn2: TBitBtn;
    procedure FormCreate(Sender: TObject);
    function InsetallEnumBack(Version:Integer):Integer;
    function UnInsetallEnumBack(Version:Integer):Integer;
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure InstallWork(AJob: PQJob);
  private
    { Private declarations }
  public
    { Public declarations }
  end;


var
  Form1: TForm1;
  ide: TIDEPacket;
  MyPath:string;

implementation

{$R *.dfm}

function GetName(version:Integer):string;
begin
    //计算版本名称
    case version of
      8: Result := 'XE';
     17: Result := '10';
    else
     if version > 17 then
       Result := '10_' + IntToStr(version - 17) // just a guess
     else if version > 13 then
       Result := 'XE' + IntToStr(2 + (version - 10))
     else
       Result := 'XE' + IntToStr(2 + (version - 9));
    end;
end;

function TForm1.InsetallEnumBack(Version:Integer):Integer;
var
  i:Integer;
  pack:string;
begin

  for I := 0 to Memo1.Lines.Count - 1 do
  begin
    ide.AddSearchPath(Version,'Win32',MyPath + Memo1.Lines.Strings[i]);
    ide.AddBrowsingPath(Version,'Win32',MyPath + Memo1.Lines.Strings[i]);
    ide.AddSearchPath(Version,'Win64',MyPath + Memo1.Lines.Strings[i]);
    ide.AddBrowsingPath(Version,'Win64',MyPath + Memo1.Lines.Strings[i]);
  end;

  ide.AddSearchPath(Version,'Win32',MyPath + 'Plugins\win32');
  ide.AddBrowsingPath(Version,'Win32',MyPath + 'Plugins\win32');
  ide.WriteTargetPlatformslistRegedit(Version);

  ide.ComponentLine.BPLOutpath := '"' + MyPath + 'Plugins\win32"';
  ide.ComponentLine.BPIOutpath := '"' + MyPath + 'Plugins\win32"';
  ide.ComponentLine.DCPOutpath := '"' + MyPath + 'Plugins\win32"';
  ide.ComponentLine.ObjOutpath := '"' + MyPath + 'Plugins\win32"';
  ide.ComponentLine.HppOutpath := '"' + MyPath + 'Plugins\win32"';

  for i := 0 to Memo2.Lines.Count - 1 do
  begin

    pack := Memo2.Lines.Strings[I] + GetName(Version) + '.dpk'; //
    Form1.BitBtn1.Caption := '编译中..' + pack;
    ide.Component32(Version,MyPath + ExtractFilePath(pack),ExtractFileName(pack),True);
    pack := Memo2.Lines.Strings[I] + GetName(Version) + '_Dsgn.dpk';
    Form1.BitBtn1.Caption := '编译中..' + pack;
    ide.Component32(Version,MyPath + ExtractFilePath(pack),ExtractFileName(pack),True);
    Application.ProcessMessages;
  end;

  Form1.BitBtn1.Caption := '安装中..' + pack;
    //复制文件到指定目录
  ide.CopyDirFilesToDir(MyPath + 'Plugins\win32','*.bpl',ide.GetUserPacketDir(Version) + 'bpl\');
  ide.CopyDirFilesToDir(MyPath + 'Plugins\win32','*.dcp',ide.GetUserPacketDir(Version) + 'dcp\');
  ide.CopyDirFilesToDir(MyPath + 'Plugins\win32','*.bpi',ide.GetUserPacketDir(Version) + 'dcp\');
  ide.CopyDirFilesToDir(MyPath + 'Plugins\win32','*.lib',ide.GetUserPacketDir(Version) + 'dcp\');

  for i := 0 to Memo2.Lines.Count - 1 do
  begin
    pack := Memo2.Lines.Strings[I] + GetName(Version) + '_Dsgn.dpk';
    ide.InsetallBPL(Version,ide.GetUserPacketDir(Version) + 'bpl\',ExtractFileName(pack),'TRichView of' + ExtractFileName(pack));
    Application.ProcessMessages;
  end;
end;

function TForm1.UnInsetallEnumBack(Version:Integer):Integer;
var
  i:Integer;
  pack:string;
begin
    for I := 0 to Memo1.Lines.Count - 1 do
    begin
      ide.DelSearchPath(Version,'Win32',MyPath + Memo1.Lines.Strings[i]);
      ide.DelBrowsingPath(Version,'Win32',MyPath + Memo1.Lines.Strings[i]);
      ide.DelSearchPath(Version,'Win64',MyPath + Memo1.Lines.Strings[i]);
      ide.DelBrowsingPath(Version,'Win64',MyPath + Memo1.Lines.Strings[i]);
    end;

    ide.DelSearchPath(Version,'Win32',MyPath + 'Plugins\win32');
    ide.DelBrowsingPath(Version,'Win32',MyPath + 'Plugins\win32');
    ide.WriteTargetPlatformslistRegedit(Version);

    for i := 0 to Memo2.Lines.Count - 1 do
    begin
      pack := Memo2.Lines.Strings[I] + GetName(Version) + '_Dsgn.dpk';
      ide.UnInsetallBPL(Version,ide.GetUserPacketDir(Version) + 'bpl\',ExtractFileName(pack));
    end;


end;

procedure TForm1.InstallWork(AJob: PQJob);
begin
  ide.EnumVersionBDS(InsetallEnumBack);
end;

procedure TForm1.BitBtn1Click(Sender: TObject);
var
  workerhandle:IntPtr;
begin
  form1.BitBtn1.Enabled := False;
  Form1.BitBtn2.Enabled := False;
  Form1.BitBtn1.Caption := '正在安装...';

  workerhandle := Workers.Post(InstallWork,nil);
  Workers.WaitJob(workerhandle,$FFFFFF,True);
  Form1.BitBtn1.Caption := '一键安装';
  form1.BitBtn1.Enabled := True;
  Form1.BitBtn2.Enabled := True;

  Application.MessageBox('安装完毕，请测试，如果没有安装成功请联系QQ：6192122', '提示', MB_OK +
      MB_ICONINFORMATION + MB_TOPMOST);

end;

procedure TForm1.BitBtn2Click(Sender: TObject);
begin
  form1.BitBtn2.Enabled := False;
  Form1.BitBtn2.Caption := '正在卸载...';
  ide.EnumVersionBDS(UnInsetallEnumBack);
  Application.MessageBox('卸载完毕', '提示', MB_OK +
    MB_ICONINFORMATION + MB_TOPMOST);
  Form1.BitBtn2.Caption := '卸载';
  form1.BitBtn2.Enabled := True;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin


  MyPath := ExtractFilePath(ParamStr(0));
  ide := TIDEPacket.Create();
  if not ide.DirWritable('c:\') then
    Application.MessageBox('建议以管理员身份运行本程序！', '提示', MB_OK + MB_ICONSTOP);

  if NOT DirectoryExists(MyPath + 'Plugins') then
    MkDir(MyPath + 'Plugins');
  if  NOT DirectoryExists(MyPath + 'Plugins\win32') then
    MkDir(MyPath + 'Plugins\win32');
  if  NOT DirectoryExists(MyPath + 'Plugins\win64') then
    MkDir(MyPath + 'Plugins\win64');
end;

end.
