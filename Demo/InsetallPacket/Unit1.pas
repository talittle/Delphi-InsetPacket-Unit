unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs,IDEInsetupPacket, Vcl.StdCtrls;

type
  TForm1 = class(TForm)
    Button1: TButton;
    Button2: TButton;
    procedure FormCreate(Sender: TObject);
    function InsetallEnumBack(Version:Integer):Integer;
    function UnInsetallEnumBack(Version:Integer):Integer;
    procedure Button1Click(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
  ide: TIDEPacket;
  MyPath:string;
  InsetOKIDE:string;



implementation

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
begin
  InsetOKIDE := '';
  ide.EnumVersionBDS(InsetallEnumBack);
  ShowMessage(InsetOKIDE);
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
  InsetOKIDE := '';
  ide.EnumVersionBDS(UnInsetallEnumBack);
  ShowMessage(InsetOKIDE);
end;

function TForm1.UnInsetallEnumBack(Version:Integer):Integer;
begin
  Result := 1;
  if Version = 19 then
  begin

    InsetOKIDE := '在以下环境中卸载成功：' + #13#10;

    //添加搜索路径
    ide.DelSearchPath(Version,'Win32',ide.GetRootPath(Version) + 'source\DIOCP');
    ide.DelSearchPath(Version,'Win64',ide.GetRootPath(Version) + 'source\DIOCP');

    //写入注册表
    ide.WriteTargetPlatformslistRegedit(Version);

    //安装控件
    if ide.UninsetallBPL(Version,ide.GetUserPacketDir(Version) + 'bpl\','diocpVcl.dpk') then
      InsetOKIDE := InsetOKIDE + ide.GetIDEName(Version) + #13#10;
  end;
end;

function TForm1.InsetallEnumBack(Version:Integer):Integer;
begin
  Result := 1;

    InsetOKIDE := '在以下环境中安装成功：' + #13#10;

    //复制源代码到目录，如果win10则IDE需要管理员权限运行,你可以选择一个
    if not DirectoryExists(ide.GetRootPath(Version) + 'source\DIOCP\') then
     ForceDirectories(ide.GetRootPath(Version) + 'source\DIOCP\');
    ide.CopyDirFilesToDir(MyPath + 'source','*.*',ide.GetRootPath(Version) + 'source\DIOCP\');

    if not DirectoryExists(ide.GetRootPath(Version) + 'source\DIOCP\pcre\') then
     ForceDirectories(ide.GetRootPath(Version) + 'source\DIOCP\pcre\');
    ide.CopyDirFilesToDir(MyPath + 'source\pcre','*.*',ide.GetRootPath(Version) + 'source\DIOCP\pcre\');

    if not DirectoryExists(ide.GetRootPath(Version) + 'source\DIOCP\win32\') then
     ForceDirectories(ide.GetRootPath(Version) + 'source\DIOCP\win32\');
    ide.CopyDirFilesToDir(MyPath + 'source\win32','*.*',ide.GetRootPath(Version) + 'source\DIOCP\win32\');

    if not DirectoryExists(ide.GetRootPath(Version) + 'source\DIOCP\win64\') then
     ForceDirectories(ide.GetRootPath(Version) + 'source\DIOCP\win64\');
    ide.CopyDirFilesToDir(MyPath + 'source\win64','*.*',ide.GetRootPath(Version) + 'source\DIOCP\win64\');

    if not DirectoryExists(ide.GetRootPath(Version) + 'source\DIOCP\zlib\') then
     ForceDirectories(ide.GetRootPath(Version) + 'source\DIOCP\zlib\');
    ide.CopyDirFilesToDir(MyPath + 'source\zlib','*.*',ide.GetRootPath(Version) + 'source\DIOCP\zlib\');

    //添加编译输出路径
    ide.ComponentLine.BPLOutpath := '"' + ide.GetRootPath(Version) + 'source\DIOCP"';

    ide.ComponentLine.DCPOutpath := '"' + ide.GetRootPath(Version) + 'source\DIOCP"';

    ide.ComponentLine.HppOutpath := '"' + ide.GetRootPath(Version) + 'source\DIOCP"';

    ide.ComponentLine.BPIOutpath := '"' + ide.GetRootPath(Version) + 'source\DIOCP"';

    ide.ComponentLine.DCUOutpath := '"' + ide.GetRootPath(Version) + 'source\DIOCP"';
    //添加搜索路径
    ide.AddSearchPath(Version,'Win32',ide.GetRootPath(Version) + 'source\DIOCP');
    ide.AddSearchPath(Version,'Win64',ide.GetRootPath(Version) + 'source\DIOCP');

    //写入注册表
    ide.WriteTargetPlatformslistRegedit(Version);
    //编译
    ide.Component(Version,ide.GetRootPath(Version) + 'source\DIOCP\','diocpVcl.dpk');
    //复制文件到指定目录
    ide.CopyDirFilesToDir(MyPath + 'source','*.bpl',ide.GetUserPacketDir(Version) + 'bpl\');
    ide.CopyDirFilesToDir(MyPath + 'source','*.dcp',ide.GetUserPacketDir(Version) + 'dcp\');
    ide.CopyDirFilesToDir(MyPath + 'source','*.bpi',ide.GetUserPacketDir(Version) + 'dcp\');
    ide.CopyDirFilesToDir(MyPath + 'source','*.lib',ide.GetUserPacketDir(Version) + 'dcp\');

   // Sleep(5000);
    //安装控件
    if ide.InsetallBPL(Version,ide.GetUserPacketDir(Version) + 'bpl\','diocpVcl.dpk') then
      InsetOKIDE := InsetOKIDE + ide.GetIDEName(Version) + #13#10;

end;


procedure TForm1.FormCreate(Sender: TObject);
begin
  MyPath := ExtractFilePath(ParamStr(0));
  ide := TIDEPacket.Create(dcc32);
  if not ide.DirWritable('c:\') then
    Application.MessageBox('请以管理员身份运行本程序！', '提示', MB_OK + MB_ICONSTOP);
end;

procedure TForm1.FormDestroy(Sender: TObject);
begin
  ide.Free;
end;

end.
