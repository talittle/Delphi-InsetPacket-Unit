unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs,IDEInsetupPacket, Vcl.StdCtrls;

type
  TForm1 = class(TForm)
    Button1: TButton;
    Button2: TButton;
    procedure FormCreate(Sender: TObject);
    function InsetallEnumBack(Version:Integer):Integer;
    function UnInsetallEnumBack(Version:Integer):Integer;
    procedure Button1Click(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
  ide: TIDEPacket;
  MyPath:string;
  InsetOKIDE:string;



implementation

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
begin
  InsetOKIDE := '';
  ide.EnumVersionBDS(InsetallEnumBack);

end;

procedure TForm1.Button2Click(Sender: TObject);
begin
  InsetOKIDE := '';
  ide.EnumVersionBDS(UnInsetallEnumBack);
 // ShowMessage(InsetOKIDE);
end;

function TForm1.UnInsetallEnumBack(Version:Integer):Integer;
begin
  Result := 1;
 // if Version = 19 then
  begin

    InsetOKIDE := '在以下环境中卸载成功：' + #13#10;

    //添加搜索路径
    ide.DelSearchPath(Version,'Win32',MyPath + 'madKernel\Sources');
    ide.DelSearchPath(Version,'Win64',MyPath + 'madKernel\Sources');
    ide.DelSearchPath(Version,'Win32',MyPath + 'madBasic\Sources');
    ide.DelSearchPath(Version,'Win64',MyPath + 'madBasic\Sources');
    ide.DelSearchPath(Version,'Win32',MyPath + 'madDisAsm\Sources');
    ide.DelSearchPath(Version,'Win64',MyPath + 'madDisAsm\Sources');
    ide.DelSearchPath(Version,'Win32',MyPath + 'madExcept\Sources');
    ide.DelSearchPath(Version,'Win64',MyPath + 'madExcept\Sources');
    ide.DelSearchPath(Version,'Win32',MyPath + 'madRemote\Sources');
    ide.DelSearchPath(Version,'Win64',MyPath + 'madRemote\Sources');
    ide.DelSearchPath(Version,'Win32',MyPath + 'madSecurity\Sources');
    ide.DelSearchPath(Version,'Win64',MyPath + 'madSecurity\Sources');
    ide.DelSearchPath(Version,'Win32',MyPath + 'madShell\Sources');
    ide.DelSearchPath(Version,'Win64',MyPath + 'madShell\Sources');
    ide.DelSearchPath(Version,'Win32',MyPath + 'Plugins\win32');
    ide.DelSearchPath(Version,'Win64',MyPath + 'Plugins\win64');

    ide.DelBrowsingPath(Version,'Win32',MyPath + 'madKernel\Sources');
    ide.DelBrowsingPath(Version,'Win64',MyPath + 'madKernel\Sources');
    ide.DelBrowsingPath(Version,'Win32',MyPath + 'madBasic\Sources');
    ide.DelBrowsingPath(Version,'Win64',MyPath + 'madBasic\Sources');
    ide.DelBrowsingPath(Version,'Win32',MyPath + 'madDisAsm\Sources');
    ide.DelBrowsingPath(Version,'Win64',MyPath + 'madDisAsm\Sources');
    ide.DelBrowsingPath(Version,'Win32',MyPath + 'madExcept\Sources');
    ide.DelBrowsingPath(Version,'Win64',MyPath + 'madExcept\Sources');
    ide.DelBrowsingPath(Version,'Win32',MyPath + 'madRemote\Sources');
    ide.DelBrowsingPath(Version,'Win64',MyPath + 'madRemote\Sources');
    ide.DelBrowsingPath(Version,'Win32',MyPath + 'madSecurity\Sources');
    ide.DelBrowsingPath(Version,'Win64',MyPath + 'madSecurity\Sources');
    ide.DelBrowsingPath(Version,'Win32',MyPath + 'madShell\Sources');
    ide.DelBrowsingPath(Version,'Win64',MyPath + 'madShell\Sources');
    ide.DelBrowsingPath(Version,'Win32',MyPath + 'Plugins\win32');
    ide.DelBrowsingPath(Version,'Win64',MyPath + 'Plugins\win64');
    //写入注册表
    ide.WriteTargetPlatformslistRegedit(Version);

    ide.DelUserOverrides(Version,'madshi');
    ide.DelUserOverrides(Version,'IdeVer');

    //安装控件
    ide.UnInsetallBPL(Version,ide.GetUserPacketDir(Version) + 'bpl\','madBasic_.dpk');
    ide.UnInsetallBPL(Version,ide.GetUserPacketDir(Version) + 'bpl\','madDisAsm_.dpk');
    ide.UnInsetallBPL(Version,ide.GetUserPacketDir(Version) + 'bpl\','madKernel_.dpk');
    ide.UnInsetallBPL(Version,ide.GetUserPacketDir(Version) + 'bpl\','madRemote_.dpk');
    ide.UnInsetallBPL(Version,ide.GetUserPacketDir(Version) + 'bpl\','madExcept_.dpk');
    ide.UnInsetallBPL(Version,ide.GetUserPacketDir(Version) + 'bpl\','madExceptIde_.dpk');
    ide.UnInsetallBPL(Version,ide.GetUserPacketDir(Version) + 'bpl\','madExceptVcl_.dpk');
    ide.UnInsetallBPL(Version,ide.GetUserPacketDir(Version) + 'bpl\','madExceptWizard_.dpk');
    ide.UnInsetallBPL(Version,ide.GetUserPacketDir(Version) + 'bpl\','madSecurity_.dpk');
    ide.UnInsetallBPL(Version,ide.GetUserPacketDir(Version) + 'bpl\','madShell_.dpk');
    ShowMessage('卸载成功！by：妖蛋');
  end;
end;

function TForm1.InsetallEnumBack(Version:Integer):Integer;
begin
    Result := 1;
    //添加搜索路径
    ide.AddSearchPath(Version,'Win32',MyPath + 'madKernel\Sources');
    ide.AddSearchPath(Version,'Win64',MyPath + 'madKernel\Sources');
    ide.AddSearchPath(Version,'Win32',MyPath + 'madBasic\Sources');
    ide.AddSearchPath(Version,'Win64',MyPath + 'madBasic\Sources');
    ide.AddSearchPath(Version,'Win32',MyPath + 'madDisAsm\Sources');
    ide.AddSearchPath(Version,'Win64',MyPath + 'madDisAsm\Sources');
    ide.AddSearchPath(Version,'Win32',MyPath + 'madExcept\Sources');
    ide.AddSearchPath(Version,'Win64',MyPath + 'madExcept\Sources');
    ide.AddSearchPath(Version,'Win32',MyPath + 'madRemote\Sources');
    ide.AddSearchPath(Version,'Win64',MyPath + 'madRemote\Sources');
    ide.AddSearchPath(Version,'Win32',MyPath + 'madSecurity\Sources');
    ide.AddSearchPath(Version,'Win64',MyPath + 'madSecurity\Sources');
    ide.AddSearchPath(Version,'Win32',MyPath + 'madShell\Sources');
    ide.AddSearchPath(Version,'Win64',MyPath + 'madShell\Sources');
    ide.AddSearchPath(Version,'Win32',MyPath + 'Plugins\win32');
    ide.AddSearchPath(Version,'Win64',MyPath + 'Plugins\win64');

    ide.AddBrowsingPath(Version,'Win32',MyPath + 'madKernel\Sources');
    ide.AddBrowsingPath(Version,'Win64',MyPath + 'madKernel\Sources');
    ide.AddBrowsingPath(Version,'Win32',MyPath + 'madBasic\Sources');
    ide.AddBrowsingPath(Version,'Win64',MyPath + 'madBasic\Sources');
    ide.AddBrowsingPath(Version,'Win32',MyPath + 'madDisAsm\Sources');
    ide.AddBrowsingPath(Version,'Win64',MyPath + 'madDisAsm\Sources');
    ide.AddBrowsingPath(Version,'Win32',MyPath + 'madExcept\Sources');
    ide.AddBrowsingPath(Version,'Win64',MyPath + 'madExcept\Sources');
    ide.AddBrowsingPath(Version,'Win32',MyPath + 'madRemote\Sources');
    ide.AddBrowsingPath(Version,'Win64',MyPath + 'madRemote\Sources');
    ide.AddBrowsingPath(Version,'Win32',MyPath + 'madSecurity\Sources');
    ide.AddBrowsingPath(Version,'Win64',MyPath + 'madSecurity\Sources');
    ide.AddBrowsingPath(Version,'Win32',MyPath + 'madShell\Sources');
    ide.AddBrowsingPath(Version,'Win64',MyPath + 'madShell\Sources');
    ide.AddBrowsingPath(Version,'Win32',MyPath + 'Plugins\win32');
    ide.AddBrowsingPath(Version,'Win64',MyPath + 'Plugins\win64');
    //写入注册表
    ide.WriteTargetPlatformslistRegedit(Version);

    ide.ComponentLine.BPLOutpath := '"' + MyPath + 'Plugins\win32"';
    ide.ComponentLine.BPIOutpath := '"' + MyPath + 'Plugins\win32"';
    ide.ComponentLine.DCPOutpath := '"' + MyPath + 'Plugins\win32"';
    ide.ComponentLine.ObjOutpath := '"' + MyPath + 'Plugins\win32"';
    ide.ComponentLine.HppOutpath := '"' + MyPath + 'Plugins\win32"';

    ide.AddUserOverrides(Version,'madshi',MyPath,False);
    ide.AddUserOverrides(Version,'IdeVer','BDS' + ide.VersionToProductVersion(Version),False);

    //编译
    ide.Component(Version,MyPath + 'madBasic\Sources','madBasic_.dpk');
    ide.Component(Version,MyPath + 'madBasic\Sources','madHelp_.dpk');
    ide.Component(Version,MyPath + 'madDisAsm\Sources','madDisAsm_.dpk');
    ide.Component(Version,MyPath + 'madKernel\Sources','madKernel_.dpk');
    ide.Component(Version,MyPath + 'madRemote\Sources','madRemote_.dpk');
    ide.Component(Version,MyPath + 'madExcept\Sources','madExcept_.dpk');
    ide.Component(Version,MyPath + 'madExcept\Sources','madExceptIde_.dpk');
    ide.Component(Version,MyPath + 'madExcept\Sources','madExceptVcl_.dpk');
    ide.Component(Version,MyPath + 'madExcept\Sources','madExceptWizard_.dpk');
    ide.Component(Version,MyPath + 'madSecurity\Sources','madSecurity_.dpk');
    ide.Component(Version,MyPath + 'madShell\Sources','madShell_.dpk');
    ide.Component(Version,MyPath + 'madCodeHook\Sources\Delphi','madCodeHook_.dpk');
    //


    //复制文件到指定目录
    ide.CopyDirFilesToDir(MyPath + 'Plugins\win32','*.bpl',ide.GetUserPacketDir(Version) + 'bpl\');
    ide.CopyDirFilesToDir(MyPath + 'Plugins\win32','*.dcp',ide.GetUserPacketDir(Version) + 'dcp\');
    ide.CopyDirFilesToDir(MyPath + 'Plugins\win32','*.bpi',ide.GetUserPacketDir(Version) + 'dcp\');
    ide.CopyDirFilesToDir(MyPath + 'Plugins\win32','*.lib',ide.GetUserPacketDir(Version) + 'dcp\');

   // Sleep(5000);
    //安装控件

    ide.InsetallBPL(Version,ide.GetUserPacketDir(Version) + 'bpl\','madBasic_.dpk');
    ide.InsetallBPL(Version,ide.GetUserPacketDir(Version) + 'bpl\','madDisAsm_.dpk');
    ide.InsetallBPL(Version,ide.GetUserPacketDir(Version) + 'bpl\','madKernel_.dpk');
    ide.InsetallBPL(Version,ide.GetUserPacketDir(Version) + 'bpl\','madRemote_.dpk');
    ide.InsetallBPL(Version,ide.GetUserPacketDir(Version) + 'bpl\','madExcept_.dpk');
    ide.InsetallBPL(Version,ide.GetUserPacketDir(Version) + 'bpl\','madExceptIde_.dpk');
    ide.InsetallBPL(Version,ide.GetUserPacketDir(Version) + 'bpl\','madExceptVcl_.dpk');
    ide.InsetallBPL(Version,ide.GetUserPacketDir(Version) + 'bpl\','madExceptWizard_.dpk');
    ide.InsetallBPL(Version,ide.GetUserPacketDir(Version) + 'bpl\','madSecurity_.dpk');
    ide.InsetallBPL(Version,ide.GetUserPacketDir(Version) + 'bpl\','madShell_.dpk');
    ide.InsetallBPL(Version,ide.GetUserPacketDir(Version) + 'bpl\','madCodeHook_.dpk');

    ShowMessage(Format('安装成功！请在系统变量中添加 %s,然后重启计算机。 by：妖蛋',[mypath + 'madExcept\Tools']));
end;


procedure TForm1.FormCreate(Sender: TObject);
begin
  MyPath := ExtractFilePath(ParamStr(0));
  ide := TIDEPacket.Create(dcc32);
  if not ide.DirWritable('c:\') then
    Application.MessageBox('请以管理员身份运行本程序！', '提示', MB_OK + MB_ICONSTOP);
end;

procedure TForm1.FormDestroy(Sender: TObject);
begin
  ide.Free;
end;

end.
