object Form1: TForm1
  Left = 0
  Top = 0
  Caption = #36890#29992#25511#20214#23433#35013#22120'V0.1-['#37197#32622#26410#21152#36733']'
  ClientHeight = 484
  ClientWidth = 697
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 8
    Width = 60
    Height = 13
    Caption = #25628#32034#36335#24452#65306
  end
  object Label2: TLabel
    Left = 8
    Top = 223
    Width = 139
    Height = 13
    Caption = 'DPK'#25991#20214#65288#25353#39034#24207#32534#35793#65289#65306
  end
  object Label3: TLabel
    Left = 8
    Top = 144
    Width = 89
    Height = 13
    Caption = 'Delphi'#31995#32479#24120#37327#65306
  end
  object Label4: TLabel
    Left = 463
    Top = 8
    Width = 53
    Height = 13
    Caption = 'IDE'#21015#34920#65306
  end
  object ListBox1: TListBox
    Left = 8
    Top = 24
    Width = 449
    Height = 81
    ItemHeight = 13
    TabOrder = 0
  end
  object Edit1: TEdit
    Left = 8
    Top = 112
    Width = 417
    Height = 21
    TabOrder = 1
    TextHint = #32477#23545#36335#24452#65292#25903#25345#24120#37327
  end
  object Button1: TButton
    Left = 432
    Top = 112
    Width = 25
    Height = 21
    Caption = #8593
    TabOrder = 2
    OnClick = Button1Click
  end
  object Edit2: TEdit
    Left = 8
    Top = 368
    Width = 417
    Height = 21
    TabOrder = 3
    TextHint = 'DPK'#30456#23545#36335#24452
  end
  object Edit3: TEdit
    Left = 8
    Top = 395
    Width = 217
    Height = 21
    TabOrder = 4
    TextHint = 'BPL\BPI\DCP'#36755#20986#36335#24452
  end
  object Edit4: TEdit
    Left = 242
    Top = 395
    Width = 217
    Height = 21
    TabOrder = 5
    TextHint = 'HPP'#36755#20986#36335#24452
  end
  object Button2: TButton
    Left = 431
    Top = 367
    Width = 26
    Height = 22
    Caption = #8593
    TabOrder = 6
    OnClick = Button2Click
  end
  object Button3: TButton
    Left = 8
    Top = 424
    Width = 137
    Height = 31
    Caption = #20445#23384#37197#32622
    TabOrder = 7
  end
  object Button4: TButton
    Left = 552
    Top = 424
    Width = 137
    Height = 31
    Caption = #23433#35013#25511#20214
    TabOrder = 8
    OnClick = Button4Click
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 465
    Width = 697
    Height = 19
    Panels = <
      item
        Text = #26412#24037#20855#20165#29992#20110#22823#37327#25511#20214#23433#35013#31616#20415#65292#36896#25104#20219#20309#29256#26435#38382#39064#19982#26412#20154#26080#20851#12290
        Width = 50
      end>
    ExplicitTop = 384
    ExplicitWidth = 467
  end
  object Edit5: TEdit
    Left = 8
    Top = 163
    Width = 217
    Height = 21
    TabOrder = 10
    TextHint = #30456#23545#36335#24452
  end
  object Edit6: TEdit
    Left = 242
    Top = 163
    Width = 217
    Height = 21
    TabOrder = 11
    TextHint = #30456#23545#36335#24452
  end
  object CheckBox1: TCheckBox
    Left = 8
    Top = 192
    Width = 281
    Height = 17
    Caption = #20197#36335#24452#21015#34920#27169#24335#28155#21152
    TabOrder = 12
  end
  object CheckListBox1: TCheckListBox
    Left = 463
    Top = 24
    Width = 226
    Height = 392
    ItemHeight = 13
    TabOrder = 13
  end
  object CheckListBox2: TCheckListBox
    Left = 8
    Top = 242
    Width = 449
    Height = 119
    ItemHeight = 13
    TabOrder = 14
  end
end
