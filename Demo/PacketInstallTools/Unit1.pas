unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ComCtrls, Vcl.StdCtrls,IDEInsetupPacket,
  Vcl.CheckLst;

type
  TForm1 = class(TForm)
    ListBox1: TListBox;
    Label1: TLabel;
    Label2: TLabel;
    Edit1: TEdit;
    Button1: TButton;
    Edit2: TEdit;
    Edit3: TEdit;
    Edit4: TEdit;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    StatusBar1: TStatusBar;
    Edit5: TEdit;
    Label3: TLabel;
    Edit6: TEdit;
    CheckBox1: TCheckBox;
    CheckListBox1: TCheckListBox;
    Label4: TLabel;
    CheckListBox2: TCheckListBox;
    procedure Button4Click(Sender: TObject);
    function EnumIde(Version:Integer):Integer;
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;
  tideInstallInfo = class
    idenumber:Integer;
  end;

var
  Form1: TForm1;
  ide:TIDEPacket;


implementation

{$R *.dfm}


function TForm1.EnumIde(Version:Integer):Integer;
var
  INFO:tideInstallInfo;
begin
   INFO := tideInstallInfo.Create;
   INFO.idenumber := Version;
   CheckListBox1.AddItem(ide.GetIDEName(Version),INFO);
   Result := 1;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  ide := TIDEPacket.Create(dcc32);
  ide.EnumVersionBDS(EnumIde);
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
  ListBox1.Items.Add(edit1.Text);
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
  CheckListBox2.Items.Add(Edit2.Text);
end;

procedure TForm1.Button4Click(Sender: TObject);
var
  i,Z:Integer;
  VERSION:Integer;
  PATH,NAME:STRING;
begin
  for Z := 0 to CheckListBox1.Count - 1 do
  begin
    if CheckListBox1.Checked[Z] = True then
    begin
      VERSION := tideInstallInfo(CheckListBox1.Items.Objects[Z]).idenumber;
      for i := 0 to ListBox1.Items.Count -1 do
      begin
         ide.AddSearchPath(VERSION,'Win32',ListBox1.Items.Strings[i]);
         ide.AddSearchPath(VERSION,'Win64',ListBox1.Items.Strings[i]);
      end;

      ide.AddUserOverrides(VERSION,Edit5.Text,Edit6.Text,CheckBox1.Checked);

      ide.ComponentLine.DCPOutpath := '"' + Edit3.Text + '"';
      ide.ComponentLine.BPLOutpath := ide.ComponentLine.DCPOutpath;
      ide.ComponentLine.BPIOutpath := ide.ComponentLine.DCPOutpath;
      ide.ComponentLine.HppOutpath := '"' + Edit4.Text + '"';

      for i := 0 to CheckListBox2.Items.Count -1 do
      begin
         PATH := ExtractFilePath(CheckListBox2.Items[I]);
         Name := ExtractFileName(CheckListBox2.Items[I]);
         ide.Component32(VERSION,PATH,NAME);
        // ide.InsetallBPL(VERSION,PATH,Name);
      end;

      ide.CopyDirFilesToDir(ide.ComponentLine.BPLOutpath,'*.bpl',ide.GetUserPacketDir(Version) + 'bpl\');
      ide.CopyDirFilesToDir(ide.ComponentLine.DCPOutpath,'*.dcp',ide.GetUserPacketDir(Version) + 'dcp\');
      ide.CopyDirFilesToDir(ide.ComponentLine.BPIOutpath,'*.bpi',ide.GetUserPacketDir(Version) + 'dcp\');
      ide.CopyDirFilesToDir(ide.ComponentLine.HppOutpath,'*.hpp',ide.GetUserPacketDir(Version) + 'dcp\');

      for i := 0 to CheckListBox2.Items.Count -1 do
      begin
         PATH := ExtractFilePath(CheckListBox2.Items[I]);
         Name := ExtractFileName(CheckListBox2.Items[I]);
         if CheckListBox2.Checked[I] then
          ide.InsetallBPL(VERSION,ide.GetUserPacketDir(Version) + 'bpl\',Name);
      end;

    end;
  end;
end;

end.
