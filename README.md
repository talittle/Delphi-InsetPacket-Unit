# Delphi InsetPacket Unit

#### 介绍(Introduction)

     为Delphi控件开发者提供轻量级控件安装包开发单元，使用该单元可以轻易的编写一个Delphi控件的安装程序。

     Provides a lightweight control installation package development unit for Delphi control developers, which can be used to easily write a Delphi control installation program.
    

#### 开发环境(Development environment)

    Embarcadero® RAD Studio 10.2

#### 参考文献(references)
    
    Embarcadero® RAD Studio 10.2
    Jvcl
    gitee.com

#### 鸣谢(Acknowledgement)

    感谢在开发中提出宝贵意见的朋友们。

    Thanks to the friends who gave valuable advice during development.

#### 开源说明(Open source instructions)
    
    您可以任意的使用该代码，但您必须遵守BSD-3-Clause协议，详情请参见LICENSE。

    You can use this code arbitrarily, but you must follow the BSD-3-Clause protocol, see LICENSE for details.

#### 定制安装程序

    如果您是Delphi控件开发者并想拥有安全可靠的安装程序，您可以选择联系作者为您编写。
